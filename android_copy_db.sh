#!/bin/bash
if [ $# -ne 3 ]; then
  echo "Usage: $0 package dbname destination"
  exit 0
fi
PACKAGE_NAME=$1
DB_NAME=$2
adb pull /data/data/${PACKAGE_NAME}/databases/${DB_NAME}.db $3
